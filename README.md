# STAr
## Stop Tracking Already
- - -
The internet is a place of infinite knowledge, but also infinite threats to your privacy and security.

The STAr project is an education project to help everyone have better internet privacy and secuity.

### "I don't have anything to hide, so why should I care about internet privacy?"

While you are not using the internet for nefarious reasons, you do still have something to hide: your confidential data. Malicious trackers and websites can scan your browser, phone cache, etc for private information like your address, fullname, financial information, and even your SSN, all without you ever even knowing. That is what you have to hide. Privacy is to protect your identity against malicious actors.

### "I don't have a lot of money, can I still have better privacy and security?"

Yes!

Finances do not effect your ability to improve privacy and security.

### "I'm sold! Where do I start?"

You can start by heading to our [wiki](https://codeberg.org/desertsasquatch/STAr/wiki).
